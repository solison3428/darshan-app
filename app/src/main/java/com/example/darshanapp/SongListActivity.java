package com.example.darshanapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.example.darshanapp.Interface.SongListInterface;
import com.example.darshanapp.adapter.SongListAdapter;
import com.example.darshanapp.model.SongListModel;
import com.example.darshanapp.util.Config;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SongListActivity extends AppCompatActivity implements SongListInterface {

    //YouTubePlayerView youTubePlayerView;


    @BindView(R.id.song_playlist_rv)
    RecyclerView song_playlist_rv;

    SongListAdapter songListAdapter;
    List<SongListModel> listOfSongs;
    LinearLayoutManager linearLayoutManager;

    String str_song_link;



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_live_video);
        ButterKnife.bind(this);


        listOfSongs = new ArrayList<>();
        songListAdapter = new SongListAdapter(listOfSongs, this, this);
        song_playlist_rv.setHasFixedSize(true);
        linearLayoutManager = new LinearLayoutManager(this);
        song_playlist_rv.setLayoutManager(linearLayoutManager);
        song_playlist_rv.setAdapter(songListAdapter);


        getSongDetails();
    }

    private void getSongDetails() {

        listOfSongs.add(new SongListModel("1","Ganesh Bhajan", "2:30", "7L0A-99DM1k"));
        listOfSongs.add(new SongListModel("2","Shiv Bhajan", "2:30", "IU6NVqHHb0U"));
    }


    @Override
    public void sendSong(String song_link) {

    }
}