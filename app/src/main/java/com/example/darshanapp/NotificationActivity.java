package com.example.darshanapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.example.darshanapp.adapter.NotificationAdapter;
import com.example.darshanapp.model.NotificationModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NotificationActivity extends AppCompatActivity {

   @BindView(R.id.notification_rv)
   RecyclerView notification_rv;

    NotificationAdapter notificationAdapter;
    List<NotificationModel> litOfNotification;
    LinearLayoutManager linearLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        ButterKnife.bind(this);

        litOfNotification = new ArrayList<>();
        notificationAdapter = new NotificationAdapter(litOfNotification, this);
        notification_rv.setHasFixedSize(true);
        linearLayoutManager = new LinearLayoutManager(this);
        notification_rv.setLayoutManager(linearLayoutManager);
        notification_rv.setAdapter(notificationAdapter);

        getNotificationList();
    }

    private void getNotificationList() {
        litOfNotification.add(new NotificationModel("1", "Aaarti Starting", "Aarti shuru hone wali hai..", "04-Mar 21, 03:44PM"));
        litOfNotification.add(new NotificationModel("1", "Aaarti Khatam", "Aarti khatam hone wali hai..", "05-Mar 21, 03:44PM"));
        litOfNotification.add(new NotificationModel("1", "Hawan Starting", "Hawan shuru hone wali hai..", "06-Mar 21, 03:44PM"));
        litOfNotification.add(new NotificationModel("1", "Hawan Khatam", "Aarti shuru hone wali hai..", "02-Mar 21, 03:44PM"));
        litOfNotification.add(new NotificationModel("1", "Puja starts tomorrow", "Aarti shuru hone wali hai..", "04-Mar 21, 03:44PM"));


    }
}