package com.example.darshanapp.fragment;

import android.graphics.Color;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.darshanapp.R;
import com.example.darshanapp.SliderImageAdapter;
import com.example.darshanapp.SliderNewModel;
import com.smarteist.autoimageslider.IndicatorView.animation.type.IndicatorAnimationType;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderView;

import java.util.ArrayList;
import java.util.List;

public class SevaFragment extends Fragment {

    SliderImageAdapter sliderViewAdapter;
    List<SliderNewModel> sliderItemList;
    @BindView(R.id.imageSlider)
    SliderView sliderView;

    public SevaFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_seva, container, false);

        ButterKnife.bind(this, view);
        sliderViewAdapter = new SliderImageAdapter(getActivity());
        sliderItemList = new ArrayList<>();

        sliderView.setSliderAdapter(sliderViewAdapter);

        sliderView.setIndicatorAnimation(IndicatorAnimationType.WORM); //set indicator animation by using SliderLayout.IndicatorAnimations. :WORM or THIN_WORM or COLOR or DROP or FILL or NONE or SCALE or SCALE_DOWN or SLIDE and SWAP!!
        sliderView.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION);
        sliderView.setAutoCycleDirection(SliderView.AUTO_CYCLE_DIRECTION_BACK_AND_FORTH);
        sliderView.setIndicatorUnselectedColor(Color.GRAY);
        sliderView.setScrollTimeInSec(4);

        sliderItemList.add(new SliderNewModel("",
                "https://yatradham.org/blog/wp-content/uploads/2020/11/Kolhapur-Mahalakshmi-Temple-REopening-.jpg"));
        sliderViewAdapter.renewItems(sliderItemList);

        return view;
    }
}