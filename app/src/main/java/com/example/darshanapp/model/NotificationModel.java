package com.example.darshanapp.model;

public class NotificationModel {

    private String notification_id;
    private String notification_heading;
    private String notification_sub_heading;
    private String notification_date;

    public NotificationModel(String notification_id, String notification_heading, String notification_sub_heading, String notification_date) {
        this.notification_id = notification_id;
        this.notification_heading = notification_heading;
        this.notification_sub_heading = notification_sub_heading;
        this.notification_date = notification_date;
    }

    public String getNotification_id() {
        return notification_id;
    }

    public void setNotification_id(String notification_id) {
        this.notification_id = notification_id;
    }

    public String getNotification_heading() {
        return notification_heading;
    }

    public void setNotification_heading(String notification_heading) {
        this.notification_heading = notification_heading;
    }

    public String getNotification_sub_heading() {
        return notification_sub_heading;
    }

    public void setNotification_sub_heading(String notification_sub_heading) {
        this.notification_sub_heading = notification_sub_heading;
    }

    public String getNotification_date() {
        return notification_date;
    }

    public void setNotification_date(String notification_date) {
        this.notification_date = notification_date;
    }
}
