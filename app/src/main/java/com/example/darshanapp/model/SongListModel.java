package com.example.darshanapp.model;

public class SongListModel {

    private String song_id;
    private String song_name;
    private String song_duration;
    private String song_link;

    public SongListModel(String song_id, String song_name, String song_duration, String song_link) {
        this.song_id = song_id;
        this.song_name = song_name;
        this.song_duration = song_duration;
        this.song_link = song_link;
    }

    public String getSong_id() {
        return song_id;
    }

    public void setSong_id(String song_id) {
        this.song_id = song_id;
    }

    public String getSong_name() {
        return song_name;
    }

    public void setSong_name(String song_name) {
        this.song_name = song_name;
    }

    public String getSong_duration() {
        return song_duration;
    }

    public void setSong_duration(String song_duration) {
        this.song_duration = song_duration;
    }

    public String getSong_link() {
        return song_link;
    }

    public void setSong_link(String song_link) {
        this.song_link = song_link;
    }
}
