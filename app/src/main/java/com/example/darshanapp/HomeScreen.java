package com.example.darshanapp;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Notification;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.os.Bundle;
import android.view.MenuItem;

import com.example.darshanapp.fragment.SandeshFragment;
import com.example.darshanapp.fragment.SangeetFragment;
import com.example.darshanapp.fragment.SevaFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeScreen extends AppCompatActivity {


    @BindView(R.id.bottom_navigation)
    BottomNavigationView bottom_navigation;
    final Fragment sevaFragment = new SevaFragment();
    final Fragment sangeetFragment = new SangeetFragment();
    final Fragment sandeshFragment = new SandeshFragment();
    final FragmentManager fm = getSupportFragmentManager();
    Fragment active = sevaFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        fm.beginTransaction().add(R.id.container_home, sandeshFragment, "sandesh").hide(sandeshFragment).commit();
        fm.beginTransaction().add(R.id.container_home, sangeetFragment, "sangeet").hide(sangeetFragment).commit();
        fm.beginTransaction().add(R.id.container_home, sevaFragment, "seva").commit();

        bottom_navigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.page_1:
                        fm.beginTransaction().hide(active).show(sevaFragment).commit();
                        active = sevaFragment;
                        return true;
                    case R.id.page_2:
                        fm.beginTransaction().hide(active).show(sangeetFragment).commit();
                        active = sangeetFragment;
                        return true;
                    case R.id.page_3:
                        fm.beginTransaction().hide(active).show(sandeshFragment).commit();
                        active = sandeshFragment;
                        return true;
                }
                return false;
            }
        });
    }

    private void openSevaFrag(){
        getSupportFragmentManager().beginTransaction().add(R.id.container_home, new SevaFragment(), "seva_frag");
    }

    private void openSangeetFrag(){
        getSupportFragmentManager().beginTransaction().replace(R.id.container_home, new SangeetFragment(), "sangeet_frag");
    }

    private void openSandeshFrag(){
        getSupportFragmentManager().beginTransaction().replace(R.id.container_home, new SandeshFragment(), "sandesh_frag");

    }

    public void openVideoActvity(View view) {
        startActivity(new Intent(HomeScreen.this, SongListActivity.class));
    }

    public void openNotification(View view) {
        startActivity(new Intent(HomeScreen.this, NotificationActivity.class));

    }
}