package com.example.darshanapp.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextClock;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.example.darshanapp.Interface.SongListInterface;
import com.example.darshanapp.PlayYouTubeSongActivity;
import com.example.darshanapp.R;
import com.example.darshanapp.model.SongListModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SongListAdapter extends RecyclerView.Adapter<SongListAdapter.ViewHolder> {

    private List<SongListModel> songListModels;
    private Context context;
    SongListInterface songListInterface;


    public SongListAdapter(List<SongListModel> songListModels, Context context, SongListInterface songListInterface) {
        this.songListModels = songListModels;
        this.context = context;
        this.songListInterface = songListInterface;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.song_list_model, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {


        SongListModel songListModel = songListModels.get(position);
        holder.tv_song_name.setText(songListModel.getSong_name());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, PlayYouTubeSongActivity.class);
                intent.putExtra("song_link", songListModels.get(position).getSong_link());
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return songListModels.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {

        @BindView(R.id.iv_song_image)
        ImageView iv_song_image;

        @BindView(R.id.tv_song_name)
        TextView tv_song_name;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
