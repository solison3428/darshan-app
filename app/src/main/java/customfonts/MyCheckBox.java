package customfonts;


import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

public class MyCheckBox extends androidx.appcompat.widget.AppCompatCheckBox {

    // Constructors
    public MyCheckBox(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }
    public MyCheckBox(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }
    public MyCheckBox(Context context) {
        super(context);
        init();
    }

    // This class requires myfont.ttf to be in the assets/fonts folder
    private void init() {
        if (!isInEditMode()) {
            Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/Lato-Regular.ttf");
            setTypeface(tf);
        }
    }
}